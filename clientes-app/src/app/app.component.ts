import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})


export class AppComponent {

    title = 'Bienvenido a Angular';

    alumno: string = 'César Vargas Escorcia';
    curso: string = 'App con Spring y Angular';
}
