import { Injectable } from '@angular/core';
import { CLIENTES } from './clientes.json';
import { Cliente } from './cliente';
import { Observable, of, throwError } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/Operators'
import swal from 'sweetalert2';

import { Router } from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class ClienteService {

    //Consumir el servicio de spring
    public urlEndPoint: string = 'http://localhost:8080/api/clientes';
    private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

    constructor(
        private http: HttpClient,
        private router: Router) { }

    getClientes(): Observable<Cliente[]> {
        //return of(CLIENTES);
        //return this.http.get<Cliente[]>(this.urlEndPoint);
        return this.http.get(this.urlEndPoint).pipe(
            map((response) => response as Cliente[]));
    }

    create(cliente: Cliente): Observable<Cliente> {
        return this.http.post(this.urlEndPoint, cliente, { headers: this.httpHeaders }).pipe(
            map((response: any) => response.cliente as Cliente),
            catchError((error) => {
                console.error(error.error.mensajes);
                swal.fire('Error al crear al cliente', error.error.mensaje, 'error');
                return throwError(error);
            })
        )
    }

    getCliente(id: any): Observable<Cliente> {
        return this.http.get<Cliente>(`${this.urlEndPoint}/${id}`).pipe(
            catchError((error) => {
                this.router.navigate(['/clientes'])
                console.log(error.error.mensaje);
                swal.fire('Error al editar', error.error.mensaje, 'error');
                return throwError(error);
            })
        );
    }

    update(cliente: Cliente): Observable<any> {
        return this.http.put<any>(`${this.urlEndPoint}/${cliente.id}`, cliente, { headers: this.httpHeaders }).pipe(
            catchError((error) => {
                console.error(error.error.mensajes);
                swal.fire('Error al editar al cliente', error.error.mensaje, 'error');
                return throwError(error);
            })
        )
    }

    delete(id: number): Observable<Cliente> {
        return this.http.delete<Cliente>(`${this.urlEndPoint}/${id}`, { headers: this.httpHeaders }).pipe(
            catchError((error) => {
                console.error(error.error.mensajes);
                swal.fire('Error al eliminar al cliente', error.error.mensaje, 'error');
                return throwError(error);
            })
        )
    }

}
