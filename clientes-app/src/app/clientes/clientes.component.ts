import { Component, Input, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';

import swal from 'sweetalert2';


@Component({
    selector: 'app-clientes',
    templateUrl: './clientes.component.html'
})
export class ClientesComponent implements OnInit {

    clientes: Cliente[] = [];

    constructor(private clientesService: ClienteService) {

    }

    ngOnInit(): void {
        this.clientesService.getClientes().subscribe(
            //función anonima
            (clientes) => this.clientes = clientes
        );
    }

    delete(cliente: Cliente): void {
        const swalWithBootstrapButtons = swal.mixin({
            customClass: {

                confirmButton: 'btn btn-success m-3',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Esta seguro?',
            text: `¿Seguro que desea eliminal al cluente ${cliente.nombre} ${cliente.apellido}`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, eliminar!',
            cancelButtonText: 'No, cancelar!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                this.clientesService.delete(cliente.id!).subscribe(
                    response => {
                        //Quitar de la lista el cliente eliminado, filter  crea un nuevo array con todos los elementos
                        this.clientes = this.clientes.filter(cli => cli !== cliente)
                        swalWithBootstrapButtons.fire(
                            'Cliente eliminado!',
                            `${cliente.nombre} eliminado con éxito`,
                            'success'
                        )
                    }
                )

            }
        })
    }

}
