import { Cliente } from './cliente';

export const CLIENTES: Cliente[] = [
    {
        id: 1,
        nombre: 'César',
        apellido: 'Vargas',
        createAt: '2021-10-17',
        email: 'cesar@correo.com',
    },
    {
        id: 2,
        nombre: 'César',
        apellido: 'Vargas',
        createAt: '2021-10-17',
        email: 'cesar@correo.com',
    },
    {
        id: 3,
        nombre: 'César',
        apellido: 'Vargas',
        createAt: '2021-10-17',
        email: 'cesar@correo.com',
    },
    {
        id: 4,
        nombre: 'César',
        apellido: 'Vargas',
        createAt: '2021-10-17',
        email: 'cesar@correo.com',
    }
]
