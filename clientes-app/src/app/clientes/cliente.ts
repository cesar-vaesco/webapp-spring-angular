export class Cliente {

    /*
    El signo ? permite que no sa necesario inicializar el atributo, de lo contrario
    el compilador de typescript nos presenta un error
    */
    id?: number;
    nombre?: string;
    apellido?: string;
    createAt?: string;
    email?: string;
}
